﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using MyNetCoreApi.Domain;
using MyNetCoreApi.Services;
using System;
using System.Threading.Tasks;

namespace MyNetCoreApi.Tests.PruebasUnitarias
{
    [TestClass]
    public class AutoresServiceTests : BasePruebas
    {
        [TestMethod]
        public async Task ObtenerAutorPorIdNoExistente()
        {
            // Preparación
            var nombreBD = Guid.NewGuid().ToString();
            var contexto = ConstruirContext(nombreBD);
            var serviceAutor = new AutorService(contexto);

            // Prueba
            var resultado = await serviceAutor.GetByIdAsync(1);

            //Verificación
            Assert.IsNull(resultado);
        }

        [TestMethod]
        public async Task ObtenerAutorPorIdExistente()
        {
            // Preparación
            var nombreBD = Guid.NewGuid().ToString();
            var contexto = ConstruirContext(nombreBD);

            contexto.Autores.Add(new Autor() { Nombre = "Autor 1", FechaNacimiento = DateTime.Now.AddYears(-33) });
            await contexto.SaveChangesAsync();

            var contexto2 = ConstruirContext(nombreBD);
            var serviceAutor = new AutorService(contexto2);

            // Prueba
            var id = 1;
            var resultado = await serviceAutor.GetByIdAsync(id);

            //Verificación
            Assert.IsNotNull(resultado);
            Assert.AreEqual(id, resultado.Id);
        }
    }
}