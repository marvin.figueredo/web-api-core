﻿using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MyNetCoreApi.Domain;
using MyNetCoreApi.Models;
using MyNetCoreApi.Services;
using System;
using System.Threading.Tasks;

namespace MyNetCoreApi.Tests.PruebasUnitarias
{
    [TestClass]
    public class GenericServiceTests : BasePruebas
    {
        [TestMethod]
        public async Task ObtenerTodosLosAutores()
        {
            var nombreBD = Guid.NewGuid().ToString();
            var contexto = ConstruirContext(nombreBD);
            var mapper = ConfigurarAutoMapper();

            contexto.Autores.Add(new Autor() { Nombre = "Autor 1", FechaNacimiento = DateTime.Now.AddYears(-33) });
            contexto.Autores.Add(new Autor() { Nombre = "Autor 2", FechaNacimiento = DateTime.Now.AddYears(-38).AddMonths(-7).AddDays(3) });
            await contexto.SaveChangesAsync();

            var contexto2 = ConstruirContext(nombreBD);
            var service = new GenericService(contexto2, mapper);

            var resultado = await service.GetAllAsync<Autor, AutorDTO>();

            var contexto3 = ConstruirContext(nombreBD);
            var cantidad = await contexto3.Autores.CountAsync();

            Assert.IsNotNull(resultado);
            Assert.AreEqual(cantidad, resultado.Count);
        }

        [TestMethod]
        public async Task AgregarNuevoAutor()
        {
            var nombreBD = Guid.NewGuid().ToString();
            var contexto = ConstruirContext(nombreBD);
            var mapper = ConfigurarAutoMapper();

            var nuevoAutor = new Autor() { Nombre = "Pepe Reina", FechaNacimiento = new DateTime(1977, 11, 22) };

            var service = new GenericService(contexto, mapper);
            var resultado = await service.AddEditAsync<Autor, AutorDTO>(nuevoAutor);

            var contexto2 = ConstruirContext(nombreBD);
            var cantidad = await contexto2.Autores.CountAsync();

            Assert.IsNotNull(resultado);
            Assert.AreEqual(1, cantidad);
        }

        [TestMethod]
        public async Task IntentarEditarAutorInexistente()
        {
            var nombreBD = Guid.NewGuid().ToString();
            var contexto = ConstruirContext(nombreBD);
            var mapper = ConfigurarAutoMapper();

            contexto.Autores.Add(new Autor() { Nombre = "Autor 1", FechaNacimiento = DateTime.Now.AddYears(-33) });
            await contexto.SaveChangesAsync();

            var autorEdit = new Autor() { Id = 99, Nombre = "Autor 1.1", FechaNacimiento = DateTime.Now.AddYears(-33) };

            var contexto2 = ConstruirContext(nombreBD);
            var service = new GenericService(contexto2, mapper);
            var resultado = await service.AddEditAsync<Autor, AutorDTO>(autorEdit);

            var contexto3 = ConstruirContext(nombreBD);
            var existe = await contexto3.Autores.AnyAsync(c => c.Nombre == "Autor 1.1");

            Assert.IsNull(resultado);
            Assert.IsFalse(existe);
        }

        [TestMethod]
        public async Task EditarAutor()
        {
            var nombreBD = Guid.NewGuid().ToString();
            var contexto = ConstruirContext(nombreBD);
            var mapper = ConfigurarAutoMapper();

            contexto.Autores.Add(new Autor() { Nombre = "Autor 1", FechaNacimiento = DateTime.Now.AddYears(-33) });
            await contexto.SaveChangesAsync();

            var autorEdit = new Autor() { Id = 1, Nombre = "Autor 1.1", FechaNacimiento = DateTime.Now.AddYears(-33) };

            var contexto2 = ConstruirContext(nombreBD);
            var service = new GenericService(contexto2, mapper);
            var resultado = await service.AddEditAsync<Autor, AutorDTO>(autorEdit);

            var contexto3 = ConstruirContext(nombreBD);
            var existe = await contexto3.Autores.AnyAsync(c => c.Nombre == "Autor 1.1");

            Assert.IsNotNull(resultado);
            Assert.IsTrue(existe);
        }

        [TestMethod]
        public async Task IntentarEliminarAutorInexistente()
        {
            var nombreBD = Guid.NewGuid().ToString();
            var contexto = ConstruirContext(nombreBD);
            var mapper = ConfigurarAutoMapper();

            var service = new GenericService(contexto, mapper);
            var resultado = await service.DeleteAsync<Autor>(1);

            Assert.AreEqual(0, resultado);
        }

        [TestMethod]
        public async Task EliminarAutor()
        {
            var nombreBD = Guid.NewGuid().ToString();
            var contexto = ConstruirContext(nombreBD);
            var mapper = ConfigurarAutoMapper();

            contexto.Autores.Add(new Autor() { Nombre = "Autor 1", FechaNacimiento = DateTime.Now.AddYears(-33) });
            await contexto.SaveChangesAsync();

            var contexto2 = ConstruirContext(nombreBD);
            var service = new GenericService(contexto2, mapper);
            var resultado = await service.DeleteAsync<Autor>(1);

            var contexto3 = ConstruirContext(nombreBD);
            var existe = await contexto3.Autores.AnyAsync();

            Assert.AreEqual(1, resultado);
            Assert.IsFalse(existe);
        }
    }
}