﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using MyNetCoreApi.Domain;
using MyNetCoreApi.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MyNetCoreApi.Tests.PruebasDeIntegracion
{
    [TestClass]
    public class AutoresControllerTests : BasePruebas
    {
        [TestMethod]
        public async Task ListaVaciaDeAutores()
        {
            var nombreBD = Guid.NewGuid().ToString();
            var factory = BuildWebApplicationFactory(nombreBD);

            var client = factory.CreateClient();
            var url = "/api/autores";
            var response = await client.GetAsync(url);

            response.EnsureSuccessStatusCode();

            var autores = JsonConvert.DeserializeObject<List<AutorDTO>>(await response.Content.ReadAsStringAsync());
            Assert.AreEqual(0, autores.Count);
        }

        [TestMethod]
        public async Task ListarTodosLosAutores()
        {
            var databaseName = Guid.NewGuid().ToString();
            var factory = BuildWebApplicationFactory(databaseName);
            var context = ConstruirContext(databaseName);
            context.Autores.Add(new Autor() { Nombre = "Juan Pérez", FechaNacimiento = new DateTime(1944, 03, 12) });
            context.Autores.Add(new Autor() { Nombre = "Larissa Aquino", FechaNacimiento = new DateTime(1990, 07, 17) });
            context.SaveChanges();

            var client = factory.CreateClient();
            var url = "/api/autores";
            var response = await client.GetAsync(url);

            response.EnsureSuccessStatusCode();

            var autores = JsonConvert.DeserializeObject<List<AutorDTO>>(await response.Content.ReadAsStringAsync());
            Assert.AreEqual(2, autores.Count);
        }

        //***Aquí los demás métodos, siguiendo el mismo criterio
    }
}