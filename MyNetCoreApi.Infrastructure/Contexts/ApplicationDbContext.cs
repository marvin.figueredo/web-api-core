﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using MyNetCoreApi.Domain;
using MyNetCoreApi.Domain.Varios;

namespace MyNetCoreApi.Infrastructure
{
    public class ApplicationDbContext: IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            :base(options)
        {

        }

        public DbSet<Autor> Autores { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            //var roleAdmin = new IdentityRole()
            //{
            //    Id = "1a5530ad-a18e-41f4-8e54-a6e0d46ebde3",
            //    Name = "admin",
            //    NormalizedName = "admin"
            //};

            //builder.Entity<IdentityRole>().HasData(roleAdmin);

            builder.Entity<LibrosAutores>().HasKey(c => new { c.LibroId, c.AutorId });

            base.OnModelCreating(builder);
        }
    }
}
