﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MyNetCoreApi.Infrastructure.Migrations
{
    public partial class Autoraddimagen : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Imagen",
                table: "Autores",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Imagen",
                table: "Autores");
        }
    }
}
