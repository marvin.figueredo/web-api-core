﻿namespace MyNetCoreApi.Models
{
    public class LibroDTO
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
    }
}
