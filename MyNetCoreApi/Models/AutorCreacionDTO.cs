﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MyNetCoreApi.Configuration;
using MyNetCoreApi.Validations;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MyNetCoreApi.Models
{
    public class AutorCreacionDTO
    {
        [Required]
        public string Nombre { get; set; }
        [Required]
        public DateTime FechaNacimiento { get; set; }
        [FileSizeValidator(MaxFileSizeInMbs: 4)]
        [ContentTypeValidator(ContentTypeGroup.Image)]
        public IFormFile Imagen { get; set; }
        [ModelBinder(BinderType = typeof(TypeBinder<List<LibroDTO>>))]
        public List<LibroDTO> Libros { get; set; }
    }
}
