﻿using AutoMapper;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MyNetCoreApi.Domain;
using MyNetCoreApi.Infrastructure;
using MyNetCoreApi.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyNetCoreApi.Controllers
{
    public class CustomBaseController: ControllerBase
    {
        private readonly ApplicationDbContext context;
        private readonly IMapper mapper;
        private readonly IGenericService service;

        public CustomBaseController(
            ApplicationDbContext context, 
            IMapper mapper,
            IGenericService service)
        {
            this.context = context;
            this.mapper = mapper;
            this.service = service;
        }

        protected async Task<List<TDTO>> Get<TEntity, TDTO>() where TEntity : class
        {
            var autores = await service.GetAllAsync<TEntity, TDTO>();
            var autoresDTO = mapper.Map<List<TDTO>>(autores);
            return autoresDTO;
        }

        protected async Task<ActionResult> Delete<TEntity>(int id) where TEntity : class, IId, new()
        {
            var entidadId = await service.DeleteAsync<TEntity>(id);

            if (entidadId == default)
            {
                return NotFound();
            }

            return NoContent();
        }

        protected async Task<ActionResult<TDTO>> Get<TEntity, TDTO>(int id, IQueryable<TEntity> queryable) where TEntity : class, IId
        {
            var entity = await queryable.AsNoTracking().FirstOrDefaultAsync(x => x.Id == id);

            if (entity == null)
            {
                return NotFound();
            }

            return mapper.Map<TDTO>(entity);
        }

        protected async Task<ActionResult> Post<TCreation, TEntity, TRead>(TCreation creation, string routeName) where TEntity : class, IId
        {
            var entity = mapper.Map<TEntity>(creation);
            var readDTO = await service.AddEditAsync<TEntity, TRead>(entity);
            return new CreatedAtRouteResult(routeName, new { entity.Id }, readDTO);
        }

        protected async Task<ActionResult> Put<TCreation, TEntity>(int id, TCreation creation) where TEntity : class, IId
        {
            var entity = await context.Set<TEntity>().FindAsync(id);
            entity = mapper.Map(creation, entity);
            await service.AddEditAsync<TEntity, TCreation>(entity);
            return NoContent();
        }

        protected async Task<ActionResult> Patch<TEntity, TDTO>(int id, JsonPatchDocument<TDTO> patchDocument) where TDTO : class
            where TEntity : class, IId
        {
            if (patchDocument == null)
            {
                return BadRequest();
            }

            var entityFromDB = await context.Set<TEntity>().FirstOrDefaultAsync(x => x.Id == id);

            if (entityFromDB == null)
            {
                return NotFound();
            }

            var entityDTO = mapper.Map<TDTO>(entityFromDB);

            patchDocument.ApplyTo(entityDTO, ModelState);

            var isValid = TryValidateModel(entityDTO);

            if (!isValid)
            {
                return BadRequest(ModelState);
            }

            mapper.Map(entityDTO, entityFromDB);

            await context.SaveChangesAsync();

            return NoContent();
        }
    }
}
