﻿using AutoMapper;
using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MyNetCoreApi.Domain;
using MyNetCoreApi.Infrastructure;
using MyNetCoreApi.Models;
using System.Collections.Generic;
using System.Threading.Tasks;
using MyNetCoreApi.Interfaces;
using System.Linq;
using System.Linq.Dynamic.Core;
using Microsoft.Extensions.Logging;

namespace MyNetCoreApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [EnableCors("PermitirApiRequest")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "admin")]
    public class AutoresController : CustomBaseController
    {
        private readonly ApplicationDbContext context;
        private readonly IMapper mapper;
        private readonly IAutorService serviceAutor;
        private readonly IGenericService service;
        private readonly IConfiguration configuration;
        private readonly ILogger<AutoresController> logger;

        public AutoresController(
            ApplicationDbContext context, 
            IMapper mapper,
            IAutorService serviceAutor,
            IGenericService service,
            IConfiguration configuration,
            ILogger<AutoresController> logger) : 
            base(context, mapper, service)
        {
            this.context = context;
            this.mapper = mapper;
            this.serviceAutor = serviceAutor;
            this.service = service;
            this.configuration = configuration;
            this.logger = logger;
        }

        [HttpGet]
        [Route("/api/test")]
        [AllowAnonymous]
        public ActionResult Test()
        {
            var mi_valor = configuration["miValor"];

            return Ok("Api ready. Valor: " + mi_valor);
        }

        // GET api/autores
        [HttpGet]
        public async Task<ActionResult<IEnumerable<AutorDTO>>> Get()
        {
            var autoresDTO = await Get<Autor, AutorDTO>();
            return autoresDTO;
        }

        // GET api/autores/5 
        [HttpGet("{id}", Name = "ObtenerAutor")]
        public async Task<ActionResult<AutorDTO>> Get(int id)
        {
            var autor = await serviceAutor.GetByIdAsync(id);

            if (autor == null)
            {
                return NotFound();
            }

            var autorDTO = mapper.Map<AutorDTO>(autor);

            return autorDTO;
        }

        [HttpGet("filter")]
        public async Task<ActionResult<List<AutorDTO>>> Filter([FromQuery] FilterAutorDTO filterAutorDTO)
        {
            var autoresQueryable = context.Autores.AsQueryable();

            if (!string.IsNullOrWhiteSpace(filterAutorDTO.Nombre))
            {
                autoresQueryable = autoresQueryable.Where(x => x.Nombre.Contains(filterAutorDTO.Nombre));
            }

            //***aqui se concatenan todos los filtros que sean necesarios

            if (!string.IsNullOrWhiteSpace(filterAutorDTO.OrderingField))
            {
                try
                {
                    autoresQueryable = autoresQueryable
                        .OrderBy($"{filterAutorDTO.OrderingField} {(filterAutorDTO.AscendingOrder ? "ascending" : "descending")}");
                }
                catch
                {
                    // log this
                    logger.LogWarning("Could not order by field: " + filterAutorDTO.OrderingField);
                }
            }

            await HttpContext.InsertPaginationParametersInResponse(autoresQueryable,
                filterAutorDTO.RecordsPerPage);

            var movies = await autoresQueryable.Paginate(filterAutorDTO.Pagination).ToListAsync();

            return mapper.Map<List<AutorDTO>>(movies);
        }

        // POST api/autores
        [HttpPost]
        public async Task<ActionResult> Post([FromForm] AutorCreacionDTO autorCreacion)
        {
            var autor = mapper.Map<Autor>(autorCreacion);

            if (autorCreacion.Imagen != null)
            {
                //***Aqui se puede aplicar cualquier lógica para almacenar
                //la imagen
                autor.Imagen = autorCreacion.Imagen.FileName;
            }

            var autorDTO = await service.AddEditAsync<Autor, AutorDTO>(autor);

            return new CreatedAtRouteResult("ObtenerAutor", new { id = autor.Id }, autorDTO);
        }

        // PUT api/autores/5
        [HttpPut("{id}")]
        public async Task<ActionResult> Put(int id, [FromBody] AutorCreacionDTO autorActualizacion)
        {
            return await Put<AutorCreacionDTO, Autor>(id, autorActualizacion);
        }

        [HttpPatch("{id}")]
        public async Task<ActionResult> Patch(int id, [FromBody] JsonPatchDocument<AutorCreacionDTO> patchDocument)
        {
            return await Patch<Autor, AutorCreacionDTO>(id, patchDocument);
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            return await Delete<Autor>(id);
        }
    }
}