﻿using AutoMapper;
using MyNetCoreApi.Domain;
using MyNetCoreApi.Domain.Varios;
using MyNetCoreApi.Models;
using System.Collections.Generic;

namespace MyNetCoreApi.Configuration
{
    public class AutoMapperProfiles : Profile
    {
        public AutoMapperProfiles()
        {
            CreateMap<Autor, AutorDTO>().
                ForMember(d => d.Books, a => a.MapFrom(MapLibrosDTO));

            CreateMap<AutorCreacionDTO, Autor>()
                .ForMember(d => d.Imagen, a => a.Ignore())
                .ForMember(d => d.LibrosAutores, a => a.MapFrom(MapLibrosAutores));

            CreateMap<Autor, AutorCreacionDTO>();

            CreateMap<LibroDTO, Libro>();
        }

        private List<LibrosAutores> MapLibrosAutores(AutorCreacionDTO autorDTO, Autor autor)
        {
            var result = new List<LibrosAutores>();

            if (autorDTO.Libros == null) return result;

            foreach (var libro in autorDTO.Libros)
            {
                result.Add(new LibrosAutores()
                {
                    Autor = autor,
                    Libro = new Libro()
                    {
                        Nombre = libro.Nombre
                    }
                });
            }

            return result;
        }

        private List<LibroDTO> MapLibrosDTO(Autor autor, AutorDTO autorDTO)
        {
            var result = new List<LibroDTO>();

            if (autor.LibrosAutores == null) return result;

            foreach (var libro in autor.LibrosAutores)
            {
                result.Add(new LibroDTO() { Id = libro.LibroId, Nombre = libro.Libro.Nombre });
            }

            return result;
        }
    }
}
