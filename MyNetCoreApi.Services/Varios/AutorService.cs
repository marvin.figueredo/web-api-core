﻿using Microsoft.EntityFrameworkCore;
using MyNetCoreApi.Domain;
using MyNetCoreApi.Infrastructure;
using MyNetCoreApi.Interfaces;
using System.Threading.Tasks;

namespace MyNetCoreApi.Services
{
    public class AutorService : IAutorService
    {
        private readonly ApplicationDbContext context;

        public AutorService(ApplicationDbContext context)
        {
            this.context = context;
        }

        public async Task<Autor> GetByIdAsync(int id)
        {
            return await context.Autores
                .Include(c => c.LibrosAutores)
                .ThenInclude(c => c.Libro)
                .FirstOrDefaultAsync(x => x.Id == id);
        }
    }
}