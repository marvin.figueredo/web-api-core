﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using MyNetCoreApi.Domain;
using MyNetCoreApi.Infrastructure;
using MyNetCoreApi.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyNetCoreApi.Services
{
    public class GenericService : IGenericService
    {
        private readonly ApplicationDbContext context;
        private readonly IMapper mapper;

        public GenericService(ApplicationDbContext context, IMapper mapper)
        {
            this.context = context;
            this.mapper = mapper;
        }

        public async Task<List<TDTO>> GetAllAsync<TEntity, TDTO>() where TEntity : class
        {
            var items = await context.Set<TEntity>().ToListAsync();
            var dtos = mapper.Map<List<TDTO>>(items);
            return dtos;
        }

        public async Task<TDTO> AddEditAsync<TEntity, TDTO>(TEntity item) where TEntity : class, IId
        {
            if (item.Id == 0)
            {
                context.Set<TEntity>().Add(item);
            }
            else
            {
                if (!await context.Set<TEntity>().AnyAsync(c => c.Id == item.Id))
                {
                    return default;
                }

                context.Entry(item).State = EntityState.Modified;
            }

            await context.SaveChangesAsync();

            var dto = mapper.Map<TDTO>(item);

            return dto;
        }

        public async Task<int> DeleteAsync<TEntity>(int id) where TEntity : class, IId, new()
        {
            var entidad = await context.Set<TEntity>().FindAsync(id);

            if (entidad == null)
            {
                return 0;
            }

            context.Remove(entidad);
            await context.SaveChangesAsync();

            return id;
        }
    }
}
