﻿using MyNetCoreApi.Domain;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MyNetCoreApi.Interfaces
{
    public interface IGenericService
    {
        Task<TDTO> AddEditAsync<TEntity, TDTO>(TEntity item) where TEntity : class, IId;
        Task<int> DeleteAsync<TEntity>(int id) where TEntity : class, IId, new();
        Task<List<TDTO>> GetAllAsync<TEntity, TDTO>() where TEntity : class;
    }
}