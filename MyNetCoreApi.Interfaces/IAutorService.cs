﻿using MyNetCoreApi.Domain;
using System.Threading.Tasks;

namespace MyNetCoreApi.Interfaces
{
    public interface IAutorService
    {
        Task<Autor> GetByIdAsync(int id);
    }
}