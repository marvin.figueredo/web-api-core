﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MyNetCoreApi.Domain.Varios
{
    public class Libro
    {
        public int Id { get; set; }
        [Required]
        [StringLength(80)]
        public string Nombre { get; set; }
        public string Descripcion { get; set; }
        public List<LibrosAutores> LibrosAutores { get; set; }
    }
}
