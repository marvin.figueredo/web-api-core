﻿using MyNetCoreApi.Domain.Varios;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MyNetCoreApi.Domain
{
    public class Autor: IId
    {
        public int Id { get; set; }
        [Required]
        [StringLength(30)]
        public string Nombre { get; set; }
        public DateTime FechaNacimiento { get; set; }
        public string Imagen { get; set; }
        public List<LibrosAutores> LibrosAutores { get; set; }
    }
}
