﻿namespace MyNetCoreApi.Domain.Varios
{
    public class LibrosAutores
    {
        public int LibroId { get; set; }
        public int AutorId { get; set; }
        public Autor Autor { get; set; }
        public Libro Libro { get; set; }
    }
}
