﻿namespace MyNetCoreApi.Domain
{
    public interface IId
    {
        public int Id { get; set; }
    }
}
